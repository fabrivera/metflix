import { ObjectId } from "bson"
import { Collection, Db, MongoClient } from "mongodb"
import Producto from "../models/producto";

var db: Db;
var collections: { productos?: Collection<Producto> } = {};

export default class ProductosService {

  static async injectDB(client: MongoClient) {
    if (db) {
      return;
    }
    try {
      await client.connect();

      db = client.db(process.env.PRODUCTOS_DB_NAME);

      collections.productos = db.collection(process.env.PRODUCTOS_COLLECTION_NAME || "");

    } catch (e) {
      console.error(e);
      throw `No se puedo establecer una conexión con la bd en ProductsDAO: ${e}`;
    }
  }

  static async getProductosByID(id: string) {
    try {
      if (!collections.productos)
        return null;

      let product = await collections.productos.findOne({ _id: new ObjectId(id) });
      return product;
    } catch (e) {
      console.error(`Ocurrió un error en getProductosByID: ${e}`)
      throw e
    }
  }
}