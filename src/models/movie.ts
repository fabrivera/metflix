export interface Id {
    $oid: string;
}

export interface Rating {
    $numberDouble: string;
}

export interface Votes {
    $numberInt: string;
}

export interface Id {
    $numberInt: string;
}

export interface Imdb {
    rating: Rating;
    votes: Votes;
    id: Id;
}

export interface Year {
    $numberInt: string;
}

export interface Metacritic {
    $numberInt: string;
}

export interface NumReviews {
    $numberInt: string;
}

export interface Meter {
    $numberInt: string;
}

export interface Viewer {
    rating: Rating;
    numReviews: NumReviews;
    meter: Meter;
}

export interface Date {
    $numberLong: string;
}

export interface Dvd {
    $date: Date;
}

export interface Critic {
    rating: Rating;
    numReviews: NumReviews;
    meter: Meter;
}

export interface Rotten {
    $numberInt: string;
}

export interface LastUpdated {
    $date: Date;
}

export interface Fresh {
    $numberInt: string;
}

export interface Tomatoes {
    website: string;
    viewer: Viewer;
    dvd: Dvd;
    critic: Critic;
    boxOffice: string;
    consensus: string;
    rotten: Rotten;
    production: string;
    lastUpdated: LastUpdated;
    fresh: Fresh;
}

export interface NumMflixComments {
    $numberInt: string;
}

export interface Released {
    $date: Date;
}

export interface Wins {
    $numberInt: string;
}

export interface Nominations {
    $numberInt: string;
}

export interface Awards {
    wins: Wins;
    nominations: Nominations;
    text: string;
}

export interface Runtime {
    $numberInt: string;
}

export interface Movie {
    _id: Id;
    fullplot: string;
    imdb: Imdb;
    year: Year;
    plot: string;
    genres: string[];
    rated: string;
    metacritic: Metacritic;
    title: string;
    lastupdated: string;
    languages: string[];
    writers: string[];
    type: string;
    tomatoes: Tomatoes;
    poster: string;
    num_mflix_comments: NumMflixComments;
    released: Released;
    awards: Awards;
    countries: string[];
    cast: string[];
    directors: string[];
    runtime: Runtime;
}